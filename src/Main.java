// (c) Bryan Owens, 11/24/15
// Note that you need to download a new version of jaunt each month,
// since the free version of jaunt this program uses expires at the end of each month
// http://jaunt-api.com/download.htm

import java.text.DecimalFormat;

import com.jaunt.NotFound;
import com.jaunt.ResponseException;
import com.jaunt.UserAgent;
import com.jaunt.component.Form;

public class Main {

	public static void main(String[] args) throws ResponseException, NotFound {

		UserAgent ua = new UserAgent();
		int n = 0;
		double curr_total_velocity = 0;
		double curr_total_acceleration = 0;
		long start_time = System.currentTimeMillis();
		double[] vals = new double[2]; // [last t, last v]

		while (true) {

			// Submitting the form:
			ua.visit("https://docs.google.com/forms/d/19FfLH0u7x4L13Uh-BoEFmnCybM4WMWG6jprDNDtrVZE/viewform");
			// System.out.println(ua.doc.findFirst("<title>").getText())
			/*
			 * Uncomment the above line of code to confirm that UserAgent is
			 * visiting the right page (debugging)
			 */
			Form form = ua.doc.getForm(0);
			form.setRadio("entry.1687000490", "Gunn High School Concert Choir");
			form.submit();

			// Keeping track of # votes:
			n++;

			// Finding velocities & acceleration:
			long end_time = System.currentTimeMillis();
			DecimalFormat df = new DecimalFormat("#.####");
			double avg_v = (0.0 + n) / ((end_time - start_time) / 1000);
			double inst_v = 1.0 / ((end_time - vals[0]) / 1000);
			double a = (inst_v - vals[1] + 0.0) / ((end_time - vals[0]) / 1000);

			if (n % 100 == 0) {
				double t_to_next100 = 100 / avg_v;
				int next1000 = n - (n % 1000) + 1000;
				double t_to_next1000 = (1000 - (n % 1000)) / avg_v;
				System.out.println("We're currently at " + n
						+ " votes. Will reach " + (n + 100)
						+ " total votes in " + df.format(t_to_next100)
						+ " more sec, or " + next1000 + " total votes in "
						+ df.format(t_to_next1000) + " sec.");
				vals[0] = end_time;
				vals[1] = inst_v;
				continue;
			}

			System.out.println("We're currently at " + n
					+ " votes. (Instant v = " + df.format(inst_v)
					+ " votes/sec, " + "Average v = " + df.format(avg_v)
					+ " votes/sec, " + "Acceleration = " + df.format(a)
					+ " votes/sec^2)");
			vals[0] = end_time;
			vals[1] = inst_v;
		}
	}
}
