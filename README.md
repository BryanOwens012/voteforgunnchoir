# VoteForGunnChoir
The local radio station KDFC is holding a contest to select one high school choir to sing live. This program helps Gunn Choir gain a few votes to secure its win.
**You need Java installed on your computer to run this program.**

### How to use this program (Windows)
1. Go to the [Releases Page](https://github.com/BryanOwens012/VoteForGunnChoir/releases)
2. Download the latest release of `VoteForGunnChoir.jar` to your Downloads folder
3. Open the program Command Prompt on your computer
4. Type `cd Downloads` into Command Prompt
5. Press enter
4. Type `java -jar ` and then type the location path of the file `VoteForGunnChoir.jar`
5. Press enter
6. Yay!
7. Stop the program by pressing CTRL-C

### How to use this program (Mac)
1. Go to the [Releases Page](https://github.com/BryanOwens012/VoteForGunnChoir/releases)
2. Download the latest release of `VoteForGunnChoir.jar` to your Downloads folder
3. Open the program Terminal on your computer
4. Type `cd Downloads` into Command Prompt
5. Press enter
4. Type `java -jar ` and then type the location path of the file `VoteForGunnChoir.jar`
5. Press enter
6. Yay!
7. Stop the program by pressing CTRL-C

### Disclaimer
This program is for educational purposes only. It is not intended to be malicious, cause harm, or help anyone gain an unfair advantage.

### Links
- [KDFC's contest page](http://www.kdfc.com/pages/22175672.php)
- [Link to vote](https://docs.google.com/forms/d/19FfLH0u7x4L13Uh-BoEFmnCybM4WMWG6jprDNDtrVZE/viewform)
- [Releases Page for this program](https://github.com/BryanOwens012/VoteForGunnChoir/releases)

